/**
1. Create an activity.js file on where to write and save the solution for the activity.
[DONE] 2. Create a new database called hotel.
[done]3. Insert a single room (insertOne method) in the rooms collection with the following details:
name - single
accommodates - 2
price - 1000
description - A simple room with all the basic necessities
rooms_available - 10
isAvailable - false
[done]4. Insert multiple rooms (insertMany method) with the following details:
name - double
accommodates - 3
price - 2000
description - A room fit for a small family going on a vacation
rooms_available - 5
isAvailable - false

name - queen
accommodates - 4
price - 4000
description - A room with a queen sized bed perfect for a simple getaway
rooms_available - 15
isAvailable - false
[done]5. Use the find method to search for a room with the name double.
[done]6. Use the updateOne method to update the queen room and set the available rooms to 0.
[done]7. Use the deleteMany method to delete all rooms that have 0 rooms available.
8. Create a git repository named s28.
9. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
10. Add the link in Boodle.
 */

//hotel database

//insert one room in rooms collection
db.rooms.insertOne({
    name: "single",
    accomodates:  2,
    price: 1000,
    description: "A simple room with all the basic necessities",
    rooms_available: 10,
    isAvailable: false
});

//insert multiple rooms in rooms collection
db.rooms.insertMany([{
    name: "double",
    accomodates:  3,
    price: 2000,
    description: "A room fit for a small family going on a vacation",
    rooms_available: 5,
    isAvailable: false
},
{
    name: "queen",
    accomodates:  4,
    price: 4000,
    description: "AA room with a queen sized bed perfect for a simple getaway",
    rooms_available: 15,
    isAvailable: false
}]
);

//search the room name "double"
db.rooms.find({name: "double"});

//update the queen room and set the available rooms to 0
db.rooms.updateOne({name:"queen"}, {
    $set: {
        rooms_available: 0
    }
});

//delete all the rooms in rooms collection
db.rooms.deleteMany({rooms_available: 0});